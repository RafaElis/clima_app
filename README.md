# Clima App

This flutter app provides the weather forecast based on your location.

- [Original design link](https://www.behance.net/gallery/108290225/Weather-App?tracking_source=search_projects%7Cweather%20app%20mobile%20ui)
### App preview
-
![](/screenshots/app-preview.mp4)

### App Screenshots

- Splash Screen:
  
  ![](/screenshots/splash-screen.png)

- Loading Screen:

  ![](/screenshots/loading-screen.png)

- Home Screen:
  
  ![](/screenshots/home-screen.png)

- Menu Screen:

  ![](/screenshots/menu-screen.png)

- Next Days Screen:
  
  ![](/screenshots/next-days-screen.png)
