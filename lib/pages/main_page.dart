import 'package:clima_app/pages/home_page.dart';
import 'package:clima_app/models/daily_weather_model.dart';
import 'package:clima_app/models/next_days_weather_model.dart';
import 'package:clima_app/widgets/drawer_widget.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  final String cityName;
  final String temperature;
  final String minTemperature;
  final String maxTemperature;
  final List<DailyWeatherModel> dailyWeather;
  final List<NextDaysWeatherModel> nextDaysWeather;

  const MainPage({
    required this.cityName,
    required this.temperature,
    required this.minTemperature,
    required this.maxTemperature,
    required this.dailyWeather,
    required this.nextDaysWeather,
    Key? key,
  }) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          const DrawerWidget(),
          HomePage(
            cityName: widget.cityName,
            temperature: widget.temperature,
            maxTemperature: widget.maxTemperature,
            minTemperature: widget.minTemperature,
            dailyWeather: widget.dailyWeather,
            nextDaysWeather: widget.nextDaysWeather,
          ),
        ],
      ),
    );
  }
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: const Text('Home'),
      ),
    );
  }
}
