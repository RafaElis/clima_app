import 'package:clima_app/pages/main_page.dart';
import 'package:clima_app/models/daily_weather_model.dart';
import 'package:clima_app/models/next_days_weather_model.dart';
import 'package:clima_app/models/weather_model.dart';
import 'package:clima_app/services/location.dart';
import 'package:clima_app/services/networking.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';

class LoadingPage extends StatefulWidget {
  const LoadingPage({Key? key}) : super(key: key);

  @override
  State<LoadingPage> createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  Future getLocationWeather() async {
    try {
      LocationService locationService = LocationService();
      String latitude = '';
      String longitude = '';
      await locationService.determinePosition();
      latitude = locationService.latitude;
      longitude = locationService.longitude;
      if (!locationService.isError) {
        final WeatherModel weatherModel =
            await NetworkHelper().getWeather(latitude, longitude);
        final response =
            await NetworkHelper().getFullWeather(latitude, longitude);

        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => MainPage(
              cityName: weatherModel.cityName,
              temperature: weatherModel.temperature,
              maxTemperature: weatherModel.maxTemperature,
              minTemperature: weatherModel.minTemperature,
              dailyWeather:
                  response['dailyWeatherModel'] as List<DailyWeatherModel>,
              nextDaysWeather: response['nextDaysWeatherModel']
                  as List<NextDaysWeatherModel>,
            ),
          ),
          (route) => false,
        );

        // Navigator.push(
        //   context,
        //   MaterialPageRoute(
        //     builder: (context) => MainPage(
        //       cityName: weatherModel.cityName,
        //       temperature: weatherModel.temperature,
        //       maxTemperature: weatherModel.maxTemperature,
        //       minTemperature: weatherModel.minTemperature,
        //       dailyWeather:
        //           response['dailyWeatherModel'] as List<DailyWeatherModel>,
        //       nextDaysWeather: response['nextDaysWeatherModel']
        //           as List<NextDaysWeatherModel>,
        //     ),
        //   ),
        // );
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  void initState() {
    super.initState();
    getLocationWeather();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.light,
      ),
      child: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: <Color>[Color(0xFF5741D7), Color(0xFF9C5CFF)],
            ),
          ),
          child: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Lottie.asset(
                  'assets/animations/loading.json',
                ),
                const Text(
                  'Carregando...',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontFamily: 'Epilogue',
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
