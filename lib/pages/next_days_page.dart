import 'package:clima_app/models/next_days_weather_model.dart';
import 'package:clima_app/utils/utility.dart';
import 'package:clima_app/widgets/max_min_temperature.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class NextDaysPage extends StatelessWidget {
  final List<NextDaysWeatherModel> nextDaysWeather;
  const NextDaysPage({
    required this.nextDaysWeather,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        leading: Material(
          color: Colors.transparent,
          borderRadius: const BorderRadius.all(Radius.circular(50)),
          child: InkWell(
            borderRadius: const BorderRadius.all(Radius.circular(50)),
            onTap: () {
              Navigator.pop(context);
            },
            child: const Icon(
              Icons.arrow_back_ios,
              color: Color(0xFF5741D7),
            ),
          ),
        ),
        title: const Text(
          'Proximos 7 dias',
          style: TextStyle(
            fontFamily: 'Nunito',
            fontWeight: FontWeight.bold,
            fontSize: 12,
            color: Color(0xFF5741D7),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                gradient: const LinearGradient(
                  colors: <Color>[
                    Color(0xFF5741D7),
                    Color(0xFF9C5CFF),
                  ],
                ),
                borderRadius: const BorderRadius.all(Radius.circular(12)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Colors.grey.shade400,
                    blurRadius: 10,
                    offset: const Offset(0, 5),
                  ),
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 32.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        const Text(
                          'Amanhã',
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(height: 10),
                        Text(
                          '${nextDaysWeather[0].temperature}°',
                          style: const TextStyle(
                            fontSize: 45,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(height: 10),
                        Row(
                          children: <Widget>[
                            MaxMinTemperature(
                              temperature:
                                  '${nextDaysWeather[0].maxTemperature}',
                              temperatureType: TemperatureType.max,
                              color: Colors.white,
                            ),
                            const SizedBox(width: 24),
                            MaxMinTemperature(
                              temperature:
                                  '${nextDaysWeather[0].minTemperature}',
                              temperatureType: TemperatureType.min,
                              color: Colors.white,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: SvgPicture.asset(
                      'assets/images/thunderstorm-showers.svg',
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: 6,
              itemBuilder: (context, index) {
                final NextDaysWeatherModel nextDayWeather =
                    nextDaysWeather[index];
                return ListTile(
                  leading: Text(
                    Utility.getFormattedDateTime(
                      '${nextDayWeather.dateFormatted}',
                      pattern: 'E',
                    ),
                  ),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      MaxMinTemperature(
                        temperature: '${nextDayWeather.maxTemperature}',
                        temperatureType: TemperatureType.max,
                        color: Colors.black,
                      ),
                      MaxMinTemperature(
                        temperature: '${nextDayWeather.minTemperature}',
                        temperatureType: TemperatureType.min,
                        color: const Color(0xFF767789),
                      ),
                    ],
                  ),
                  trailing: Image.network(
                    'https://openweathermap.org/img/wn/10d@2x.png',
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
