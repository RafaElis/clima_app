import 'package:clima_app/models/daily_weather_model.dart';
import 'package:clima_app/models/next_days_weather_model.dart';
import 'package:clima_app/pages/next_days_page.dart';
import 'package:clima_app/utils/utility.dart';
import 'package:clima_app/widgets/weather_information_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomePage extends StatefulWidget {
  final String cityName;
  final String temperature;
  final String minTemperature;
  final String maxTemperature;
  final List<DailyWeatherModel> dailyWeather;
  final List<NextDaysWeatherModel> nextDaysWeather;

  const HomePage({
    required this.cityName,
    required this.temperature,
    required this.minTemperature,
    required this.maxTemperature,
    required this.dailyWeather,
    required this.nextDaysWeather,
    Key? key,
  }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String getTodayDay() {
    DateTime now = DateTime.now();
    return Utility.getFormattedDateTime(now.toString());
  }

  double xOffset = 0;
  bool isOpen = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      transform: Matrix4.translationValues(xOffset, 0, 0.0),
      child: GestureDetector(
        onTap: () => setState(() {
          isOpen = false;
          xOffset = 0;
        }),
        child: Scaffold(
          body: AnimatedContainer(
            duration: const Duration(milliseconds: 250),
            decoration: BoxDecoration(
              gradient: const LinearGradient(
                colors: <Color>[
                  Color(0xFF5741D7),
                  Color(0xFF9C5CFF),
                ],
              ),
              borderRadius:
                  isOpen ? const BorderRadius.all(Radius.circular(35)) : null,
              boxShadow: isOpen
                  ? <BoxShadow>[
                      const BoxShadow(
                        color: Color(0xFF5741D7),
                        offset: Offset(0, 5),
                        blurRadius: 50,
                      ),
                    ]
                  : null,
            ),
            child: SafeArea(
              bottom: false,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              widget.cityName,
                              style: const TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontFamily: 'Epilogue',
                              ),
                            ),
                            // SizedBox(height: 10),
                            Text(
                              getTodayDay(),
                              style: const TextStyle(
                                fontFamily: 'Nunito',
                                color: Color(0xFFD7D7DA),
                              ),
                            ),
                          ],
                        ),
                        IconButton(
                          icon: Icon(
                            isOpen ? Icons.close : Icons.menu,
                            size: 40,
                          ),
                          color: Colors.white,
                          onPressed: () {
                            if (!isOpen) {
                              isOpen = true;
                              xOffset =
                                  -MediaQuery.of(context).size.width * 0.7;
                            } else {
                              isOpen = false;
                              xOffset = 0;
                            }
                            setState(() {});
                          },
                        ),
                      ],
                    ),
                  ),
                  SvgPicture.asset(
                    'assets/images/thunderstorm-showers.svg',
                    semanticsLabel: 'Acme Logo',
                    width: 350,
                    height: 350,
                  ),
                  const Spacer(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          const Icon(
                            Icons.keyboard_arrow_up,
                            size: 14,
                            color: Colors.white,
                          ),
                          Text(
                            '${widget.maxTemperature}°',
                            style: const TextStyle(
                              fontFamily: 'Nunito',
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(width: 25),
                      Text(
                        '${widget.temperature}°',
                        style: const TextStyle(
                          fontSize: 60,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                      const SizedBox(width: 16),
                      Row(
                        children: <Widget>[
                          const Icon(
                            Icons.keyboard_arrow_down,
                            size: 14,
                            color: Colors.white,
                          ),
                          Text(
                            '${widget.minTemperature}°',
                            style: const TextStyle(
                              fontFamily: 'Nunito',
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  const Spacer(),
                  Container(
                    padding: const EdgeInsets.only(
                      top: 25,
                      left: 20,
                      right: 20,
                      bottom: 35,
                    ),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                    ),
                    width: double.infinity,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 5,
                          width: 50,
                          decoration: BoxDecoration(
                            color: const Color(0xFF4A79F3),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        const SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            const Text(
                              'Hoje',
                              style: TextStyle(
                                fontFamily: 'Nunito',
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF9795a3),
                              ),
                            ),
                            GestureDetector(
                              onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => NextDaysPage(
                                    nextDaysWeather: widget.nextDaysWeather,
                                  ),
                                ),
                              ),
                              child: const Text(
                                'Proximos 7 dias',
                                style: TextStyle(
                                  fontFamily: 'Nunito',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12,
                                  color: Color(0xFF5741D7),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            ...List.generate(5, (int index) {
                              return WeatherInformationContainer(
                                weather: widget.dailyWeather[index],
                                isSelected: index == 0,
                              );
                            }),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
