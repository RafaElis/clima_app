import 'package:flutter/material.dart';

enum TemperatureType {
  max,
  min,
}

class MaxMinTemperature extends StatelessWidget {
  final String temperature;
  final TemperatureType temperatureType;
  final Color? color;

  const MaxMinTemperature({
    required this.temperature,
    required this.temperatureType,
    this.color = Colors.white,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Icon(
          temperatureType == TemperatureType.max
              ? Icons.keyboard_arrow_up
              : Icons.keyboard_arrow_down,
          size: 14,
          color: color,
        ),
        const SizedBox(width: 4),
        Text(
          '$temperature°',
          style: TextStyle(
            fontSize: 14,
            fontFamily: 'Nunito',
            color: color,
          ),
        ),
      ],
    );
  }
}
