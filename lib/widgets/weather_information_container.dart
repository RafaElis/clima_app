import 'package:clima_app/models/daily_weather_model.dart';
import 'package:clima_app/utils/utility.dart';
import 'package:flutter/material.dart';

class WeatherInformationContainer extends StatelessWidget {
  final bool isSelected;
  final DailyWeatherModel weather;
  const WeatherInformationContainer({
    required this.isSelected,
    required this.weather,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 8,
        vertical: 4,
      ),
      decoration: BoxDecoration(
        gradient: isSelected
            ? const LinearGradient(
                colors: <Color>[
                  Color(0xFF5741D7),
                  Color(0xFF9C5CFF),
                ],
              )
            : null,
        borderRadius: BorderRadius.circular(10),
        border: isSelected
            ? null
            : Border.all(
                color: const Color(0xFF9795a3),
                width: 1,
              ),
        boxShadow: isSelected
            ? <BoxShadow>[
                BoxShadow(
                  color: Colors.grey.shade400,
                  blurRadius: 10,
                  offset: const Offset(0, 5),
                ),
              ]
            : null,
      ),
      child: Column(
        children: <Widget>[
          Text(
            Utility.getFormattedDateTime(
              '${weather.dateFormatted}',
              pattern: 'jm',
            ),
            style: TextStyle(
              fontFamily: 'Nunito',
              fontSize: 10,
              color: isSelected ? Colors.white : const Color(0xFF9795a3),
            ),
          ),
          const SizedBox(height: 8),
          Image.network(
            'https://openweathermap.org/img/wn/${weather.weather?[0].icon}.png',
            width: 35,
            height: 35,
          ),
          // Icon(
          //   isSelected ? Icons.cloud : Icons.cloud_outlined,
          //   color: isSelected ? Colors.white : const Color(0xFF9795a3),
          // ),
          const SizedBox(height: 8),
          Text(
            weather.temperature,
            style: TextStyle(
              fontFamily: 'Nunito',
              fontSize: 10,
              color: isSelected ? Colors.white : const Color(0xFF9397a4),
            ),
          ),
        ],
      ),
    );
  }
}
