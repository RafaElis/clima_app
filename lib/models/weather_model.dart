class WeatherModel {
  String cityName;
  String temperature;
  String minTemperature;
  String maxTemperature;
  WeatherModel({
    required this.cityName,
    required this.temperature,
    required this.minTemperature,
    required this.maxTemperature,
  });

  @override
  String toString() {
    return 'WeatherModel{cityName: $cityName, temperature: $temperature, minTemperature: $minTemperature, maxTemperature: $maxTemperature}';
  }
}
