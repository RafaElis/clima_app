class Temperature {
  num? day;
  num? min;
  num? max;
  num? night;
  num? eve;
  num? morn;

  Temperature({
    this.day,
    this.min,
    this.max,
    this.night,
    this.eve,
    this.morn,
  });

  Temperature.fromJson(Map<String, dynamic> json)
      : day = json['day'],
        min = json['min'],
        max = json['max'],
        night = json['night'],
        eve = json['eve'],
        morn = json['morn'];

  Map<String, dynamic> toJson() => {
        'day': day,
        'min': min,
        'max': max,
        'night': night,
        'eve': eve,
        'morn': morn,
      };
}

class FeelsLikeTemperature {
  num? day;
  num? night;
  num? eve;
  num? morn;

  FeelsLikeTemperature({
    this.day,
    this.night,
    this.eve,
    this.morn,
  });

  FeelsLikeTemperature.fromJson(Map<String, dynamic> json)
      : day = json['day'] as double,
        night = json['night'] as double,
        eve = json['eve'] as double,
        morn = json['morn'] as double;

  Map<String, dynamic> toJson() => {
        'day': day,
        'night': night,
        'eve': eve,
        'morn': morn,
      };
}

class WeatherDescription {
  int? id;
  String? main;
  String? description;
  String? icon;

  WeatherDescription({
    this.id,
    this.main,
    this.description,
    this.icon,
  });

  WeatherDescription.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    main = json['main'];
    description = json['description'];
    icon = json['icon'];
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'main': main,
        'description': description,
        'icon': icon,
      };
}
