import 'package:clima_app/models/weather_information_model.dart';

class DailyWeatherModel {
  int? dt;
  num? moonPhase;
  num? temp;
  num? feelsLike;
  int? pressure;
  int? humidity;
  num? dewPoint;
  num? windSpeed;
  int? windDeg;
  num? windGust;
  List<WeatherDescription>? weather;
  int? clouds;
  num? pop;

  DailyWeatherModel({
    this.dt,
    this.moonPhase,
    this.temp,
    this.feelsLike,
    this.pressure,
    this.humidity,
    this.dewPoint,
    this.windSpeed,
    this.windDeg,
    this.windGust,
    this.weather,
    this.clouds,
    this.pop,
  });

  DailyWeatherModel.fromJson(Map<String, dynamic> json)
      : dt = json['dt'],
        moonPhase = json['moonPhase'],
        temp = json['temp'],
        feelsLike = json['feelsLike'],
        pressure = json['pressure'],
        humidity = json['humidity'],
        dewPoint = json['dewPoint'],
        windSpeed = json['windSpeed'],
        windDeg = json['windDeg'],
        windGust = json['windGust'],
        weather = json['weather'] != null
            ? List<WeatherDescription>.from(
                json['weather'].map((x) => WeatherDescription.fromJson(x)))
            : null,
        clouds = json['clouds'],
        pop = json['pop'];

  DateTime get dateFormatted => DateTime.fromMillisecondsSinceEpoch(dt! * 1000);

  String get temperature => '${temp!.toStringAsFixed(1)}°';
}
