import 'package:clima_app/models/weather_information_model.dart';

class NextDaysWeatherModel {
  int? dt;
  int? sunrise;
  int? sunset;
  int? moonrise;
  int? moonset;
  num? moonPhase;
  Temperature? temp;
  FeelsLikeTemperature? feelsLike;
  int? pressure;
  int? humidity;
  num? dewPoint;
  num? windSpeed;
  int? windDeg;
  num? windGust;
  List<WeatherDescription>? weather;
  int? clouds;
  num? pop;
  num? uvi;
  num? rain;

  NextDaysWeatherModel({
    this.dt,
    this.sunrise,
    this.sunset,
    this.moonrise,
    this.moonset,
    this.moonPhase,
    this.temp,
    this.feelsLike,
    this.pressure,
    this.humidity,
    this.dewPoint,
    this.windSpeed,
    this.windDeg,
    this.windGust,
    this.weather,
    this.clouds,
    this.pop,
    this.uvi,
    this.rain,
  });

  NextDaysWeatherModel.fromJson(Map<String, dynamic> json)
      : dt = json['dt'],
        sunrise = json['sunrise'],
        sunset = json['sunset'],
        moonrise = json['moonrise'],
        moonset = json['moonset'],
        moonPhase = json['moonPhase'],
        temp = json['temp'] != null ? Temperature.fromJson(json['temp']) : null,
        feelsLike = json['feelsLike'] != null
            ? FeelsLikeTemperature.fromJson(json['feelsLike'])
            : null,
        pressure = json['pressure'],
        humidity = json['humidity'],
        dewPoint = json['dewPoint'],
        windSpeed = json['windSpeed'],
        windDeg = json['windDeg'],
        windGust = json['windGust'],
        weather = json['weather'] != null
            ? List<WeatherDescription>.from(
                json['weather'].map((x) => WeatherDescription.fromJson(x)))
            : null,
        clouds = json['clouds'],
        pop = json['pop'],
        uvi = json['uvi'],
        rain = json['rain'];

  DateTime get dateFormatted => DateTime.fromMillisecondsSinceEpoch(dt! * 1000);

  DateTime get sunriseFormatted =>
      DateTime.fromMillisecondsSinceEpoch(sunrise! * 1000);

  DateTime get moonriseFormatted =>
      DateTime.fromMillisecondsSinceEpoch(dt! * 1000);

  DateTime get moonsetFormatted =>
      DateTime.fromMillisecondsSinceEpoch(dt! * 1000);

  String? get temperature => temp!.day!.toStringAsFixed(0);
  String? get maxTemperature => temp!.max!.toStringAsFixed(0);
  String? get minTemperature => temp!.min!.toStringAsFixed(0);
}
