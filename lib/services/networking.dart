import 'package:clima_app/models/daily_weather_model.dart';
import 'package:clima_app/models/next_days_weather_model.dart';
import 'package:clima_app/models/weather_model.dart';
import 'package:dio/dio.dart';

const String apiKey = 'ee5cab689acc009171b08be4f102d5ea';

class NetworkHelper {
  Dio dio = Dio(BaseOptions(
    baseUrl: 'https://api.openweathermap.org/data/2.5/',
    connectTimeout: 5000,
    receiveTimeout: 3000,
    headers: {
      'Content-Type': 'application/json',
    },
    queryParameters: {
      'appid': apiKey,
      'units': 'metric',
    },
  ));

  Future<WeatherModel> getWeather(String latitude, String longitude) async {
    Response response = await dio.get('weather', queryParameters: {
      'lat': latitude,
      'lon': longitude,
    });
    if (response.statusCode == 200) {
      Map<String, dynamic> data = response.data;
      num temperature = data['main']['temp'];
      num minTemperature = data['main']['temp_min'];
      num maxTemperature = data['main']['temp_max'];
      final WeatherModel weatherModel = WeatherModel(
        cityName: data['name'],
        temperature: temperature.toStringAsFixed(1),
        minTemperature: minTemperature.toStringAsFixed(1),
        maxTemperature: maxTemperature.toStringAsFixed(1),
      );
      return weatherModel;
    } else {
      throw Exception('Failed to load internet');
    }
  }

  Future<Map<String, dynamic>> getWeatherByCity(String cityName) async {
    Response response = await dio.get('weather', queryParameters: {
      'q': cityName,
      'lang': 'pt_br',
    });
    if (response.statusCode == 200) {
      Map<String, dynamic> data = response.data;
      int condition = data['weather'][0]['id'];
      double temperature = data['main']['temp'];
      double minTemperature = data['main']['temp_min'];
      double maxTemperature = data['main']['temp_max'];
      final WeatherModel weatherModel = WeatherModel(
        cityName: data['name'],
        temperature: temperature.toStringAsFixed(1),
        minTemperature: minTemperature.toStringAsFixed(1),
        maxTemperature: maxTemperature.toStringAsFixed(1),
      );
      return {
        'condition': condition,
        'temperature': weatherModel.temperature,
        'minTemperature': weatherModel.minTemperature,
        'maxTemperature': weatherModel.maxTemperature,
        'city': weatherModel.cityName,
      };
    } else {
      throw Exception('Failed to load internet');
    }
  }

  Future<Map<String, List>> getFullWeather(
    String latitude,
    String longitude,
  ) async {
    Response response = await dio.get('onecall', queryParameters: {
      'lat': latitude,
      'lon': longitude,
      'exclude': 'minutely,alerts'
    });
    if (response.statusCode == 200) {
      List<dynamic> data = response.data['hourly'];
      List<dynamic> nextDaysData = response.data['daily'];
      List<DailyWeatherModel> dailyWeatherModel = [];
      List<NextDaysWeatherModel> nextDaysWeatherModel = [];
      for (var i = 0; i < data.length; i++) {
        dailyWeatherModel.add(DailyWeatherModel.fromJson(data[i]));
      }
      for (var i = 0; i < nextDaysData.length; i++) {
        nextDaysWeatherModel
            .add(NextDaysWeatherModel.fromJson(nextDaysData[i]));
      }
      return {
        'dailyWeatherModel': dailyWeatherModel,
        'nextDaysWeatherModel': nextDaysWeatherModel
      };
    } else {
      throw Exception('Failed to load internet');
    }
  }
}
