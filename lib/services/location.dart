import 'package:geolocator/geolocator.dart';

class LocationService {
  String _latitude = "";
  String _longitude = "";
  bool _isError = false;

  String get latitude => _latitude;
  String get longitude => _longitude;
  bool get isError => _isError;

  Future<void> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    try {
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        _isError = true;
        throw Future.error('Location services are disabled.');
      }
      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          _isError = true;
          throw Future.error('Location permissions are denied');
        } else {
          Position position = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.high,
          );
          _latitude = position.latitude.toString();
          _longitude = position.longitude.toString();
        }
      } else {
        Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.low,
        );
        _latitude = position.latitude.toString();
        _longitude = position.longitude.toString();
      }

      if (permission == LocationPermission.deniedForever) {
        _isError = true;
        throw Future.error(
          'Location permissions are permanently denied, we cannot request permissions.',
        );
      }
    } catch (e) {
      _isError = true;
      rethrow;
    }
  }
}
