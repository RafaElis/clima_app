import 'package:intl/intl.dart';

class Utility {
  static String capitalize(String input) {
    final List<String> splitStr = input.split(' ');
    for (int i = 0; i < splitStr.length; i++) {
      splitStr[i] =
          '${splitStr[i][0].toUpperCase()}${splitStr[i].substring(1)}';
    }
    final output = splitStr.join(' ');
    return output;
  }

  static String getFormattedDateTime(
    String time, {
    String? pattern = "EEEE dd',' MMMM",
  }) {
    DateFormat formatter = DateFormat(pattern, 'pt_Br');
    return capitalize(formatter.format(DateTime.parse(time)));
  }
}
